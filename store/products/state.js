export default () => ({
  booked : {
    b1: {
      id: 'b1',
      title: "Step behind",
      price: 7956,
      comments: "Step behind the scenes of Havana's theater",
      details: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum distinctio inventore nobis quisquam rem repudiandae voluptatibus. A aliquam nam possimus quod. Accusantium facilis hic molestiae mollitia numquam quam quidem sunt.",
      picture: "product_1.jpg",
    },
    b2: {
      id: 'b2',
      title: "Dance your",
      price: 7956,
      comments: "Dance your way into Cuban culture",
      details: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum distinctio inventore nobis quisquam rem repudiandae voluptatibus. A aliquam nam possimus quod. Accusantium facilis hic molestiae mollitia numquam quam quidem sunt.",
      picture: "product_2.jpg"
    },
    b3: {
      id: 'b3',
      title: "Hunt for",
      price: 13361,
      comments: "Hunt for truffles in the countryside",
      details: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum distinctio inventore nobis quisquam rem repudiandae voluptatibus. A aliquam nam possimus quod. Accusantium facilis hic molestiae mollitia numquam quam quidem sunt.",
      picture: "product_3.jpg"
    },
    b4 : {
      id: 'b4',
      title: "Meet Kenyan",
      price: 23204,
      comments: "Meet Kenyan innovators at Nairobi",
      details: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum distinctio inventore nobis quisquam rem repudiandae voluptatibus. A aliquam nam possimus quod. Accusantium facilis hic molestiae mollitia numquam quam quidem sunt.",
      picture: "product_4.jpg"
    },
    b5 : {
      id: 'b5',
      title: "Explore Miami",
      price: 13232,
      comments: "Explore Miami's Latin flavor with food",
      details: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum distinctio inventore nobis quisquam rem repudiandae voluptatibus. A aliquam nam possimus quod. Accusantium facilis hic molestiae mollitia numquam quam quidem sunt.",
      picture: "product_5.jpg"
    },
  },
  featured : {
    f1: {
      id: 'f1',
      title: "Step behind",
      price: 7956,
      comments: "Step behind the scenes of Havana's theater",
      details: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum distinctio inventore nobis quisquam rem repudiandae voluptatibus. A aliquam nam possimus quod. Accusantium facilis hic molestiae mollitia numquam quam quidem sunt.",
      picture: "product_6.jpg"
    },
    f2: {
      id: 'f2',
      title: "Dance your",
      price: 7956,
      comments: "Dance your way into Cuban culture",
      details: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum distinctio inventore nobis quisquam rem repudiandae voluptatibus. A aliquam nam possimus quod. Accusantium facilis hic molestiae mollitia numquam quam quidem sunt.",
      picture: "product_7.jpg"
    },
    f3: {
      id: 'f3',
      title: "Hunt for",
      price: 13361,
      comments: "Hunt for truffles in the conutryside",
      details: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum distinctio inventore nobis quisquam rem repudiandae voluptatibus. A aliquam nam possimus quod. Accusantium facilis hic molestiae mollitia numquam quam quidem sunt.",
      picture: "product_8.jpg"
    },
    f4: {
      id: 'f4',
      title: "Meet Kenyan",
      price: 23204,
      comments: "Meet Kenyan innovators at Nairobi",
      details: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum distinctio inventore nobis quisquam rem repudiandae voluptatibus. A aliquam nam possimus quod. Accusantium facilis hic molestiae mollitia numquam quam quidem sunt.",
      picture: "product_9.jpg"
    },
    f5: {
      id: 'f5',
      title: "Explore Miami",
      price: 13232,
      comments: "Explore Miami's Latin flavor with food",
      details: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum distinctio inventore nobis quisquam rem repudiandae voluptatibus. A aliquam nam possimus quod. Accusantium facilis hic molestiae mollitia numquam quam quidem sunt.",
      picture: "product_10.jpg"
    },
  }
})
