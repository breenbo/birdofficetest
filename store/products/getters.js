export default {
  featured(state) {
    return state.featured
  },
  booked(state) {
    return state.booked
  },
  allProducts(state) {
    return {...state.featured, ...state.booked}
  },
  idProductArray(state, getters) {
    // return array of ids to validate routes
    return Object.keys(getters.allProducts)
  }
}
