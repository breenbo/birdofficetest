export default {
  // payload: {product: {product}, number: 12}
  // command : [number: 2, product: {id: , }]
  addToCommand(state, payload) {
    const commandsId = state.command.map(el => el.product.id)
    // 1. check if id already in command array
    // 1.2. if => add number to existing number
    if (commandsId.includes(payload.product.id)) {
      // find the existing product in command
      state.command.forEach(el => {
        if (el.product.id === payload.product.id) {
          el.number += payload.number
        }
      })
    } else {
      // 2. else => push new object
      state.command.push(payload)
    }
  },
  // payload = product id to remove from command array
  removeFromCommand(state, payload) {
    const index = state.command.findIndex(el => el.product.id === payload)
    // remove from command array at index
    state.command.splice(index, 1)
  }
}
