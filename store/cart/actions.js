export default {
  addToCommand(context, payload) {
    context.commit('addToCommand', payload)
  },
  removeFromCommand(context, payload) {
    context.commit('removeFromCommand', payload)
  }
}
