export default {
  commands(state) {
    return state.command
  },
  numberOfCommand(state) {
    return state.command.reduce((acc, cur) => {
      return acc + cur.number
    }, 0)
  },
  totalCost(state) {
    return state.command.reduce((acc, cur) => {
      return acc + cur.number * cur.product.price
    }, 0)
  }
}
