# Bird Office technical test.

Project created with **Nuxt**, **Vuex**, and **Boostrap**.

Simple online store with add/change/remove from cart functionalities.
Full responsive, customized bootstrap with scss variables.

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```


